
# Step2: serve app with nginx server
# Use official nginx image as the base image
FROM nginx:latest
# Copy the build output to replace the default nginx contents.
# be sure to replace app-name with name of your app
COPY /dist/my-component /usr/share/nginx/html
# Expose port 80
EXPOSE 80
