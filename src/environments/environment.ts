// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB7CSMQ1xxOK-77jngjzfR6z7mDBGK-D0Y",
    authDomain: "ang-fire-prod-54856.firebaseapp.com",
    databaseURL: "https://ang-fire-prod-54856-default-rtdb.firebaseio.com",
    projectId: "ang-fire-prod-54856",
    storageBucket: "ang-fire-prod-54856.appspot.com",
    messagingSenderId: "178817022022",
    appId: "1:178817022022:web:7eed673193073ecf47f247",
    measurementId: "G-PHPB5QGVSX"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
