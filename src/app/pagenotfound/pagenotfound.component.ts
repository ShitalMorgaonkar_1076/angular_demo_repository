import { style } from '@angular/animations';
import { Container } from '@angular/compiler/src/i18n/i18n_ast';
import { AfterContentInit, AfterViewInit, Component, ContentChild, ElementRef, HostListener,
   OnChanges, OnInit, Renderer2, SimpleChanges, ViewChild } from '@angular/core';
import { Card2Component } from '../card2/card2.component';
import * as $ from 'jquery';

@Component({
  selector: 'app-pagenotfound',
  templateUrl: './pagenotfound.component.html',
  styleUrls: ['./pagenotfound.component.css']
})
export class PagenotfoundComponent implements OnInit, OnChanges , AfterViewInit {
  @ViewChild('box') box!: ElementRef; 
  @ViewChild(Card2Component) card2!: Card2Component;
  
  



  constructor(private renderer: Renderer2) 
  { }



  ngAfterViewInit(): void {
    // this.box.nativeElement.style.backgroundColor = 'red';
    // this.box.nativeElement.classList = "Container";
    // this.box.nativeElement.innerHTML ="this is modified"
   //renderer2 use
    this.renderer.setStyle(this.box.nativeElement,"backgroundColor",'green');
    this.renderer.addClass(this.box.nativeElement,'newclass');
    this.renderer.setAttribute(this.box.nativeElement,'value','dfd');
  }



  ngOnChanges(changes: SimpleChanges): void {
    
  }
 
 
  ngOnInit(): void {
    //add scroll function in ngonit this windowfunction will get triggered from all components 
    // $(window).scroll(function(){
    //   });
  }
 

  Value: string ='';
  comp1Exist : boolean = true;
  
  
  submitValue(val:any):void{
    this.Value = val.value;
  }


  destroycomponent(){
    this.comp1Exist = !this.comp1Exist;
  }
  

  onchangechildproperty(){
    this.card2.alertfromparent ="double thanks";
  }
  

  onchangechildmethod(){
      this.card2.btnclick();
  }

  //Hostlistener event here host is pagenotfoundcomponent
  @HostListener('click') myclick(): void{
      alert('clicked');
  }

  //method of scroll using hostlistener so that it will trigger only in this component

  @HostListener('window:scroll',['$event'])myscroll():void{
  }

}




