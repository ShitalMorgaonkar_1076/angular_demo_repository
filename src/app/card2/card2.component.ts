import { AfterContentChecked, AfterContentInit, Component, ContentChild, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { TestdirectiveDirective } from '../appDirectives/testdirective.directive';
import { DesignUtilityService } from '../appServices/design-utility.service';
import { MessageService } from '../appServices/message.service';

@Component({
  selector: 'app-card2',
  templateUrl: './card2.component.html',
  styleUrls: ['./card2.component.css']
})
export class Card2Component implements OnInit, AfterContentInit {

  @ContentChild('childcontentvariable')childcontent!:ElementRef;
  alertfromparent:string ='thank you parent';

  //to use directive in user component use viewchild to access methods of directives
  @ViewChild(TestdirectiveDirective) testdirective! :TestdirectiveDirective

  constructor(private _msgService:DesignUtilityService, private renderer : Renderer2) { }


  ngAfterContentInit(): void {
    this.renderer.setStyle(this.childcontent.nativeElement, 'color','red');
    
  }
  
  ngOnInit(): void {
  }

  // onNewssettler():void{
  //   alert('thank you for subscribe');
  // }


  btnclick(){
    // const msg = new MessageService();
    // msg.onNewssettler();
    // this._msgService.onNewssettler();
    alert(this.alertfromparent);
   
    var text = this.renderer.createText('new added text');
    this.renderer.appendChild(this.childcontent.nativeElement, text);
  }


  changedirectivecolor(value:string):void{
    this.testdirective.changebgcolorinDirective(value);
  }

}
