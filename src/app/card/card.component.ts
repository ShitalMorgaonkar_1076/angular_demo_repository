import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, DoCheck, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { DesignUtilityService } from '../appServices/design-utility.service';
import { MessageService } from '../appServices/message.service';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit, OnChanges, DoCheck , AfterContentInit, AfterContentChecked,
AfterViewInit, AfterViewChecked, OnDestroy{

  counter: any;
  num :number = 1;

  constructor(private _msgService:DesignUtilityService) {
   }
  ngOnDestroy(): void {
    // clearInterval(this.counter);
  }
  ngAfterViewChecked(): void {
  }
  ngAfterViewInit(): void {
  }
  ngAfterContentChecked(): void {
  }
  ngAfterContentInit(): void {
  }
  ngDoCheck(): void {
  }
  
  product_service='test';
  @Input() myValue = 'UXtrendz'  //checking hookcycles in pagenotfound nav bar
  ngOnInit(): void {
    // this.product_service = this._msgService.product;
    // this.counter=setInterval(()=>{
    //   this.num = this.num+1;
    // },1000);
  }
  ngOnChanges(changes: SimpleChanges): void {
  }
   
  @Input() iscontactpage:boolean = false;
  
  // onNewssettler():void{
  //   alert('thank you for subscribe');
  // }


  btnclick():void{
    // const msg = new MessageService();
    // msg.onNewssettler();
    this._msgService.onNewssettler();
  }
  
}
