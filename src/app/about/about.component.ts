import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { from, of , map , mergeAll, mergeMap} from 'rxjs';
import { forkJoin , zip } from 'rxjs';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  value = "this is my test value for pipe";
  constructor(private router: Router,
    private changeDetection : ChangeDetectorRef) { }

  getData(data: any){
    return of([data + " uploaded from getdata"]);
  }

  ngOnInit(): void {
      const source = from( ['Tech', 'Audio', 'Video']);
      const number = from([1,2,3]);
      //map example need to subscribe two time
      source.pipe(
        map(res=> this.getData(res))
      ).subscribe(response => response.subscribe(innerresponse =>{
      }));

        //merge all
      source.pipe(
          map(res=> this.getData(res)),
          mergeAll()
        ).subscribe(response => {
        });

        //mergeMAp
      source.pipe(
        mergeMap(res=> this.getData(res)),
        
      ).subscribe(response => {
      });
      
      //two observable call at same time using forkjoin return last value 
      forkJoin([source, number]).subscribe(([Response1, Response2]) =>{
      });

      //two observable call at same time using forkjoin return last value 
      zip([source, number]).subscribe(([Response1, Response2]) =>{
      });
  }
  statusonline :boolean = true;
  namesearch: string ='';
  // productarray =[
  //   {name:'laptop'},
  //   {name:'Mobile'},
  //   {name:'TV'},
  //   {name:'Fan'},
  // ]
  //for three value table object name price availability
  productarray:any =[
    {sno:'1',  name:'laptop', price:'25000', availability: 'not available'},
    {sno:'2', name:'Mobile', price:'8000', availability: 'available'},
    {sno:'3', name:'TV', price:'300', availability: 'available'}
  ]
  onclickofknowmore(e:any):void{
    this.router.navigate(['product/'+e.target.value]);
  }
  
onAddproduct(addproductvalue: string):void{
  console.log('value:',addproductvalue);
  this.productarray.push ({sno:'5', 
  name:addproductvalue,
   price:'25000', 
   availability: 'not available'});
   this.changeDetection.detectChanges();
  console.log('productarray:',this.productarray);
 }

}
function forkjoin(arg0: (import("rxjs").Observable<string> | import("rxjs").Observable<number>)[]) {
  throw new Error('Function not implemented.');
}



