import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ngfor',
  templateUrl: './ngfor.component.html',
  styleUrls: ['./ngfor.component.css']
})
export class NgforComponent implements OnInit {

  //product array of object
  product=[
    { proimage : 'assets/Laptop.png', name : 'Laptop', id:'01', price: 10000},
    { proimage : 'assets/TV.png', name : 'TV', id:'02', price: 10000},
    { proimage : 'assets/Mobile.png', name : 'Mobile', id:'03', price: 10000},
    { proimage : 'assets/Fan.png', name : 'Fan', id:'04', price: 10000},
  ]
  
  constructor(private router : Router) { }

  ngOnInit(): void {
  }


 //for push n splice on userdefined dynaic array
  users= new Array() ;
  
  onCreateUser(uname: string):void{
    //push add element in array here users object has key as name
    this.users.push({name:uname});

    //if user array length more than 3 navigate to product component
    if(this.users.length > 3){
      alert('added users are more than 3');
      this.router.navigate(['product'])
    }
  }
  onRemoveUser():void{
    //splice method require number and count how many need to delete if not given taken as 1
    this.users.splice(this.users.length-1);
  }
  removeindexeduser(i:number):void{
    this.users.splice(i,1);
  }

}
