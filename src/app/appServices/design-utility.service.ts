import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DesignUtilityService {

  constructor(private http :HttpClient) { }
  // product =[{name:'laptop' , Id:10000},
  // {name:'TV' , Id:20},
  // {name:'wifi' , Id:1000},
  // ];

  url='https://jsonplaceholder.typicode.com/users'; 
  //define product by using api from jsonplaceholder
  product():Observable<any>{
    return this.http.get(this.url);
  }

  // userName= new Subject<any>()
  userName = new BehaviorSubject('SSS');



  onNewssettler():void{
    alert('thank you for subscribe');
  }


}
