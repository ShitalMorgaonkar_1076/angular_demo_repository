import { Component, OnInit } from '@angular/core';
import { DesignUtilityService } from '../appServices/design-utility.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _designutilityservice: DesignUtilityService) {
    this._designutilityservice.userName.subscribe(username =>{
      this.userName = username;
    });

   }

  ngOnInit(): void {
  }
  userName: string ='Uxtrendz';
}
