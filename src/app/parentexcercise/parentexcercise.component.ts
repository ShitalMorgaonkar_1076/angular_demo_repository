import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parentexcercise',
  templateUrl: './parentexcercise.component.html',
  styleUrls: ['./parentexcercise.component.css']
})
export class ParentexcerciseComponent implements OnInit {

  //create array of user and admin
  users = new Array();
  admins = new Array();
  showlistuser: boolean = false;
  showlistadmin: boolean = false;
  usercount:number = 0;
  admincount:number = 0;
  constructor() { }

  ngOnInit(): void {
  }
  onadditionofuser(uservalue:string):void{
    this.users.push({name :uservalue});
    this.usercount = this.users.length;
    if(this.users.length === 3)
    {
      this.showlistuser = true;
    }
  }

  onadditionofadmin(adminvalue:string):void{
    this.admins.push({name :adminvalue});
    this.admincount = this.admins.length;
    if(this.admins.length === 3)
    {
      this.showlistadmin=true;
    }
  }

  onremoveUser(i:number){
    this.users.splice(i,1);
    this.usercount = this.users.length;
  }

  onremoveAdmin(i:number){
    this.admins.splice(i,1);
    this.admincount = this.admins.length;
  }
}
