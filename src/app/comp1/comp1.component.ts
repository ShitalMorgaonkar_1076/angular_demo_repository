import { Component, OnInit } from '@angular/core';
import { DesignUtilityService } from '../appServices/design-utility.service';

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.css']
})
export class Comp1Component implements OnInit {

  constructor(private _designutilityservice: DesignUtilityService) {
    this._designutilityservice.userName.subscribe(username =>{
      this.userName = username;
    });
   }

  ngOnInit(): void {
  }
  userName: string ='Uxtrendz';
  onupdateuname(uname : any){
    this.userName = uname.value;
    this._designutilityservice.userName.next(uname.value);
  
  };
}
