import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: true
})
export class FilterPipe implements PipeTransform {

  transform(value: any, searchitem:string) {
    return value.filter(function(search:any){
      console.log('search.name.indexOf(searchitem):',search.name.indexOf(searchitem));
      return search.name.indexOf(searchitem) > -1;
    });
  }

} 
