import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-childexcercise',
  templateUrl: './childexcercise.component.html',
  styleUrls: ['./childexcercise.component.css']
})
export class ChildexcerciseComponent implements OnInit {
  @Input() placeholderText="abc";
  @Input() Usercount = 0;
  @Input() Admincount = 0;

  @Output() onaddeduser = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }
  onCreate(inputvalue:string){
    this.onaddeduser.emit(inputvalue)
  }
}
