import { Component, OnInit, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  @Input() PlaceholderText = "abc"

  @Input() productSelected: boolean = false;
  @Input() selectedProduct:string='';
  
  @Output() addedproductinchild  = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }
  onAddedProduct():void{
    this.addedproductinchild.emit(this.selectedProduct);
 }
}
