
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MyContainerComponent } from './myContainer/myContainer.component';
import { TopnavComponent } from './myContainer/topnav/topnav.component';
import { HeaderComponent } from './myContainer/header/header.component';
import { EventbindingComponent } from './eventbinding/eventbinding.component';

//for using ngModel import formsmodule
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//ngx-bootstrap
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgforComponent } from './ngfor/ngfor.component';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { AboutComponent } from './about/about.component';
import { ProductComponent } from './product/product.component';
import { LaptopComponent } from './product/laptop/laptop.component';
import { MobileComponent } from './product/mobile/mobile.component';
import { TVComponent } from './product/tv/tv.component';
import { FanComponent } from './product/fan/fan.component';
import { CardComponent } from './card/card.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { ContactComponent } from './contact/contact.component';
import { Card2Component } from './card2/card2.component';
import { DesignUtilityService } from './appServices/design-utility.service';
import { ParentexcerciseComponent } from './parentexcercise/parentexcercise.component';
import { ChildexcerciseComponent } from './childexcercise/childexcercise.component';

//httpmodule to use api
import {HttpClientModule } from '@angular/common/http';
import { Comp1Component } from './comp1/comp1.component';
import { Comp2Component } from './comp2/comp2.component';
import { TestdirectiveDirective } from './appDirectives/testdirective.directive';
import { UxPipe } from './appPipes/ux.pipe';
import { FilterPipe } from './appPipes/filter.pipe';
//create variable of type array for Routing
const approute : Routes =[
  //redirection of route
   {path:'', redirectTo:'login', pathMatch:'full' },
  //{ path : '', component: HomeComponent}, default route
  { path : 'home', component: HomeComponent},
  { path : 'login', component: LoginComponent},
  { path : 'about', component: AboutComponent},
  // { path : 'product', component: ProductComponent},
  // child route
  // { path : 'product', children:[
  //   { path : '', component: ProductComponent},
  //   { path : 'laptop', component: LaptopComponent},
  //   { path : 'mobile', component: MobileComponent},
  //   { path : 'TV', component: TVComponent},
  //   { path : 'fan', component: FanComponent},
  // ]},
  //nested  route
  { path : 'product', component: ProductComponent,children:[
    { path : 'laptop', component: LaptopComponent},
    { path : 'mobile', component: MobileComponent},
    { path : 'TV', component: TVComponent},
    { path : 'fan', component: FanComponent},
  ]},
  { path : 'ngforcomponent', component : NgforComponent},
  { path : 'eventbindingcomponent', component : EventbindingComponent},
  { path : 'mycontainercomponent', component : MyContainerComponent},
  { path : 'ngforcomponent', component : NgforComponent},
  { path : 'eventbindingcomponent', component : EventbindingComponent},
  { path : 'mycontainercomponent', component : MyContainerComponent},
  { path : 'buy-product', component : ParentComponent},
  { path : 'contact', component : ContactComponent},
  //wild card route it always placed at last
  { path : '**', component : PagenotfoundComponent},
]

 

@NgModule({
  declarations: [
    AppComponent,
    MyContainerComponent,
    TopnavComponent,
    HeaderComponent,
    EventbindingComponent,
    NgforComponent,
    HomeComponent,
    LoginComponent,
    PagenotfoundComponent,
    AboutComponent,
    ProductComponent,
    LaptopComponent,
    MobileComponent,
    TVComponent,
    FanComponent,
    CardComponent,
    ParentComponent,
    ChildComponent,
    ContactComponent,
    Card2Component,
    ParentexcerciseComponent,
    ChildexcerciseComponent,
    Comp1Component,
    Comp2Component,
    TestdirectiveDirective,
    UxPipe,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BsDatepickerModule,
    RouterModule.forRoot(approute),
    HttpClientModule,
    BrowserAnimationsModule
  ],
  
  providers: [DesignUtilityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
