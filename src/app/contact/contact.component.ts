import { Component, OnInit } from '@angular/core';
import { DesignUtilityService } from '../appServices/design-utility.service';
import { MessageService } from '../appServices/message.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  products_list!: { name: string; id: number; }[];

  constructor(private _msgService:DesignUtilityService) { }

  ngOnInit(): void {
    // this.products_list = this._msgService.product();

    //need to subscribe product methos from service

    this._msgService.product()
    .subscribe(apiresponse => 
      this.products_list = apiresponse
      );
  }
  
}
