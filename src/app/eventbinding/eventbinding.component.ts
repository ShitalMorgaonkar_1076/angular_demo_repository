import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-eventbinding',
  templateUrl: './eventbinding.component.html',
  styleUrls: ['./eventbinding.component.css']
})
export class EventbindingComponent implements OnInit {

  constructor() {

  }
  
  ngOnInit(): void {
    
  }
  //property binding
  defaultvalue: boolean = false
  title: string = 'Shital'

  //string interpolation
  msg: string ='initial msg';
  onclick(event: any): void {
    this.msg = event.target.value + "   product added to cart"
  }

  getinputinfo(inputinfo: any) {
  }

  //two way data binding
  uname: string = "two way data binding";

  //ngIf example
  isValid: boolean = false;

  onCreateblock(): void {
    this.isValid = !this.isValid;
  }

 //ng switch example
 selectedProduct:string=' ';
 getProductvalue(e:any):void{
   this.selectedProduct = e.target.value;
 }
}
