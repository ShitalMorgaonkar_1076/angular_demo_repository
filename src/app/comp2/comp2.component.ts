import { Component, OnInit } from '@angular/core';
import { DesignUtilityService } from '../appServices/design-utility.service';

@Component({
  selector: 'app-comp2',
  templateUrl: './comp2.component.html',
  styleUrls: ['./comp2.component.css']
})
export class Comp2Component implements OnInit {

  constructor(private _designutilityservice: DesignUtilityService) {
    this._designutilityservice.userName.subscribe(username =>{
      this.userName = username;
    });
   }

  ngOnInit(): void {
  }
  userName: string ='Uxtrendz';
  onupdateuname(uname : any){
    this._designutilityservice.userName.next(uname.value);
  } 
}
