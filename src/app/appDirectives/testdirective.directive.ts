import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appTestdirective]'
})
export class TestdirectiveDirective {

  constructor(private renderer:Renderer2, private el :ElementRef) { 
    // this.renderer.setStyle(this.el.nativeElement,'backgroundColor','red');
  }

      
  changebgcolorinDirective(color:string){
    this.renderer.setStyle(this.el.nativeElement,'backgroundColor',color);
  }

  @HostListener('click') myClick(){
    alert('clicked in directive');
  }
  // @HostListener('mouseover') mymouseover(){
  //   alert('mouseover in directive');
  // }
  // @HostListener('mouseout') mymouseout(){
  //   alert('mouseout in directive');
  // }
}

