import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  //prevcode
  // proSelected_parent: boolean = false;
  // selProduct_parent:string='';
  proSelected_parent: boolean = false;
  selProduct_parent:string='';
  addedProduct:any;

  //selected product will show in downside row
  onSelectProduct(selecteditem:string):void{
    this.proSelected_parent = true;
    //will get that selected product for which we clicked
    //select product button
    this.selProduct_parent = selecteditem;
  }

  //when user click on add product selected one  goes into alert 
  //no need to pass parameter
  onAddedProduct():void{
    //in added product we will get selected product for which
    //we clicked add to cart
    //this.addedProduct = this.selProduct_parent;
  }

  parentaddedproductmethod(event:any):void{
    this.addedProduct = event;
  }
}
